<?php

/**
 * @file
 * Contains \Drupal\metatags_quick\Plugin\Field\FieldFormatter\MetatagFormatter.
 */

namespace Drupal\metatags_quick\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'metatags_quick' formatter.
 *
 * @FieldFormatter(
 *   id = "metatags_quick",
 *   label = @Translation("Meta tag"),
 *   field_types = {
 *     "metatags_quick"
 *   }
 * )
 */
class MetatagFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = array();
    foreach ($items as $delta => $item) {
      $value = $item->getValue();
      $elements['#attached']['drupal_add_html_head'][] = array(
        array(
          '#tag' => 'meta',
          '#attributes' => array(
            'name' => $this->fieldDefinition->settings['meta_name'],
            'content' => $value['value'],
          ),
        ),
        'metatags_quick_' . $item->getFieldDefinition()->get('field_name'),
      );
      /*_metatags_quick_add_head(array(
        'type' => 'meta',
        'name' => $this->fieldDefinition->settings['meta_name'],
        'content' => $value['value'],
      ));*/
    }

    return $elements;
  }
}
