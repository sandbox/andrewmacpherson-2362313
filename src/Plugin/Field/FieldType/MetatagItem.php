<?php

/**
 * @file
 * Contains \Drupal\metatags_quick\Plugin\Field\FieldType\MetatagItem.
 */

namespace Drupal\metatags_quick\Plugin\Field\FieldType;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'metatags_quick' field type.
 *
 * @FieldType(
 *   id = "metatags_quick",
 *   label = @Translation("Meta tag"),
 *   description = @Translation("This field stores meta tags."),
 *   instance_settings = {
 *     "meta_name" = "",
 *   },
 *   default_widget = "metatags_quick_default",
 *   default_formatter = "string"
 * )
 */
class MetatagItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Meta tag'));
      // @todo ->setDescription(t(''));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'varchar',
          'length' => 1024,
          'not null' => FALSE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }
}
