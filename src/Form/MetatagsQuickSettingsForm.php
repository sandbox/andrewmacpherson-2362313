<?php

/**
 * @file
 * Contains \Drupal\metatags_quick\Form\MetatagsQuickSettingsForm.
 */

namespace Drupal\metatags_quick\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Component\Utility\String;
use Drupal\field\Entity\FieldInstanceConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Configure metatags_quick settings for this site.
 */
class MetatagsQuickSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'metatags_quick_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('metatags_quick.settings');

    //$current_settings = $config->get('metatags_quick_settings', _metatags_quick_settings_default());
    $module_path = drupal_get_path('module', 'metatags_quick');
    $fields = entity_load_multiple_by_properties('field_storage_config', array('type' => 'metatags_quick'));

    include_once $module_path . '/known_tags.inc';
    $known_tags = _metatags_quick_known_fields();

    $form['global'] = array(
      '#type' => 'details',
      '#title' => t('Global settings'),
      '#collapsible' => TRUE,
      '#open' => FALSE,
    );

    if (empty($fields)) {
      $form['global']['basic_init'] = array(
        '#markup' => t('No meta tags found in your installation') . '<br/>',
      );
    }

//    $form['global']['use_path_based'] = array(
//      '#type' => 'checkbox',
//      '#title' => t('Use path-based meta tags'),
//      '#default_value' => $config->get('metatags_quick_use_path_based', 1),
//      '#return_value' => 1,
//    );
//    $form['global']['remove_tab'] = array(
//      '#type' => 'checkbox',
//      '#title' => t('Hide Path-based Metatags tab'),
//      '#default_value' => $config->get('metatags_quick_remove_tab', 0),
//    );
    $form['global']['default_field_length'] = array(
      '#type' => 'textfield',
      '#title' => t('Default maximum length'),
      '#description' => t('Default maximum length for the meta tag fields'),
      '#default_value' => $config->get('metatags_quick_default_field_length', 255),
    );
    $form['global']['show_admin_hint'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show entities/bundles hint in admin'),
      '#default_value' => $config->get('metatags_quick_show_admin_hint', 1),
    );

    $form['standard_tags'] = array(
      '#type' => 'details',
      '#title' => t('Create and attach'),
      '#description' => t('The following meta tags are known to the module and can be created automatically. However, you are not limited to this list and can define tags of your own using the Fields UI.'),
      '#collapsible' => TRUE,
      '#open' => TRUE,
      '#attached' => array(
        'js' => array($module_path . '/js/admin.js'),
        'css' => array($module_path . '/css/admin.css'),
      ),
    );
    if ($config->get('metatags_quick_show_admin_hint', 1)) {
      $form['standard_tags']['hint'] = array(
        '#prefix' => '<div class="messages messages--status">',
        '#markup' => t('<strong>Hint</strong>: press on entity type name to edit individual bundles settings (you can hide this hint in global settings).'),
        '#suffix' => '</div>',
      );
    }

    $field_instances = entity_load_multiple('field_config');

    // Build the sortable table header.
    $header = array(
      'title' => array('data' => t('Bundle/entity')),
    );
    foreach ($known_tags as $name => $tag) {
      $header[$name] = $tag['title'];
    }
    //$header['_select_all'] = t('Select all');

    foreach (entity_get_bundles() as $entity_type => $bundles) {
      $entity_info = \Drupal::entityManager()->getDefinition($entity_type);

      // We're only interested in content entities.
      if (!\Drupal::entityManager()->getStorage($entity_type) instanceof DynamicallyFieldableEntityStorageInterface) {
        continue;
      }
      $options[$entity_type]['data'] = array(
        'title' => array(
          'class' => array('entity_type_collapsible', 'entity_type_collapsed', "entity_name_$entity_type"),
          'data' => String::checkPlain($entity_info->getLabel()),
        )
      );
      foreach ($known_tags as $name => $tag) {
        $bundle_workable[$name] = FALSE;
        $options[$entity_type]['data'][$name] = array(
          'data' => array(
            '#type' => 'checkbox',
            '#attributes' => array('class' => array('cb_bundle_parent', "cb_bundle_name_{$entity_type}_{$name}")),
            '#return_value' => 1,
            '#checked' => FALSE,
          ),
        );
      }

      // How do we mark that specific meta is already attached to bundle.
      $checked_markup = array(
        '#markup' => _theme(
          'image',
          array(
            'uri' => 'core/misc/icons/73b355/check.svg',
            'width' => 18,
            'height' => 18,
            'alt' => 'ok',
            'title' => 'ok',
          )),
      );

      foreach ($bundles as $key => $bundle) {
        // Which meta tags are set for this bundle?
        $meta_set = array();
        foreach ($field_instances as $instance) {
          if ($key != $instance->bundle) {
            continue;
          }

          $field_info = FieldStorageConfig::loadByName($instance->get('entity_type'), $instance->get('field_name'));
          if ($field_info->get('module') == 'metatags_quick') {
            $meta_set[$instance->settings['meta_name']] = 1;
          }

          $options[$entity_type . '_' . $key] = array(
            'class' => array('entity_type_children', "entity_child_$entity_type"),
            'style' => 'display: none',
            'data' => array(
              'title' => array(
                'class' => array('entity_type_child_title'),
                'data' => $bundle['label'],
              ),
            ),
          );

          foreach ($known_tags as $name => $tag) {
            $tag_name = isset($tag['meta_name']) ? $tag['meta_name'] : $name;
            if (empty($meta_set[$tag_name])) {
              // Mark parent bundle as workable - display checkbox.
              $bundle_workable[$name] = TRUE;
              $options[$entity_type . '_' . $key]['data'][$name] = array(
                'data' => array(
                  '#name' => $entity_type . '[' . $key . '][' . $name . ']',
                  '#type' => 'checkbox',
                  '#attributes' => array('class' => array('cb_bundle_child', "cb_child_{$entity_type}_{$name}")),
                  '#return_value' => 1,
                  '#checked' => FALSE,
                )
              );
            }
            else {
              $options[$entity_type . '_' . $key]['data'][$name]['data'] = $checked_markup;
            }
          }
        }
      }

      // Now check if we have completely set bundles.
      foreach ($known_tags as $name => $tag) {
        if ($meta_set[$name] || !$bundle_workable[$name]) {
          $options[$entity_type]['data'][$name]['data'] = $checked_markup;
        }
      }
    }

    $form['standard_tags']['existing'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#empty' => t('No content available.'),
    );

    $form['standard_tags']['basic_init_op'] = array(
      '#type' => 'submit',
      '#value' => t('Attach'),
    );
//    $form['op'] = array(
//      '#value' => t('Submit'),
//      '#type' => 'submit',
//    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->get('values');


    $this->config('metatags_quick.settings')
      ->set('metatags_quick_use_path_based', $values['use_path_based'])
      ->set('metatags_quick_remove_tab', $values['remove_tab'])
      ->set('metatags_quick_default_field_length', $values['default_field_length'])
      ->set('metatags_quick_show_admin_hint', $values['show_admin_hint'])
      ->save();

    $triggering_element = $form_state->get('triggering_element');

    if ($triggering_element['#value'] == t('Attach') || $triggering_element['#value'] == t('Save configuration')) {
      $this->fieldsCreateAttach($form_state->get('input'));
    }
    else {
    }
//    drupal_set_message(t('Meta tags (quick) settings saved'), 'status');

    parent::submitForm($form, $form_state);
  }

  protected function fieldsCreateAttach($input) {
    foreach (entity_get_bundles() as $entity_type => $bundles) {
      if (!\Drupal::entityManager()->getStorage($entity_type) instanceof DynamicallyFieldableEntityStorageInterface) {
        continue;
      }

      foreach ($bundles as $key => $bundle) {
        if (isset($input[$entity_type][$key])) {
          foreach ($input[$entity_type][$key] as $meta_name => $meta_value) {
            $this->fieldAttach($entity_type, $key, $meta_name);
          }
        }
      }
    }
  }

  protected function fieldAttach($entity_type, $bundle, $meta_name) {
    // @todo: convert static vars to class members?
    static $meta_fields = FALSE;
    static $field_id = array();
    static $known_tags = FALSE;

    // Get metatags_quick fields info
    if (!$meta_fields) {
      include_once drupal_get_path('module', 'metatags_quick') . '/known_tags.inc';
      $known_tags = _metatags_quick_known_fields();

      $fields = entity_load_multiple_by_properties('field_storage_config', array('type' => 'metatags_quick'));
      foreach($fields as $name => $field_info) {
        $meta_fields[$field_info->id()] = $field_info;
        $field_id[$field_info->id()] = $field_info->id();
      }
    }

    // Ignore unknown tags.
    if (!isset($known_tags[$meta_name])) {
      return;
    }
    // Check if meta field exists, create if necessary.
    $meta_name_real = empty($known_tags[$meta_name]['meta_name']) ? $meta_name : $known_tags[$meta_name]['meta_name'];

    if (empty($field_id[$entity_type . '.meta_' . $meta_name])) {
      $field = array(
        'name' => "meta_$meta_name",
        'type' => 'metatags_quick',
        'entity_type' => $entity_type,
        // 'translatable' => $values['translatable'],
      );

      // Create the field.
      $field = \Drupal::entityManager()->getStorage('field_storage_config')->create($field);
      $field->save();
      $field_id[$field->id()] = $field->id();
      $meta_fields[$entity_type . '.meta_' . $meta_name_real] = $field;
    }
    else {
      $field = $meta_fields[$entity_type . '.meta_' . $meta_name_real];
    }

    $instance = FieldInstanceConfig::loadByName($entity_type, $bundle, $field->get('name'));

    // Do nothing if instance already exists.
    if ($instance instanceof FieldInstanceConfig) {
      return;
    }

    // Now create field instance attached to requested bundle.
    $instance = array(
      'field_name' => $field->get('name'),
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => '(Meta)' . $known_tags[$meta_name]['title'],
      'settings' => array('meta_name' => $meta_name_real),
    );
    $new_instance = \Drupal::entityManager()->getStorage('field_config')->create($instance);
    $new_instance->save();

    // Make sure the field is displayed in the 'default' form mode (using
    // default widget and settings). It stays hidden for other form modes
    // until it is explicitly configured.
    entity_get_form_display($entity_type, $bundle, 'default')
      ->setComponent($field->get('name'))
      ->save();

    // Make sure the field is displayed in the 'default' view mode (using
    // default formatter and settings). It stays hidden for other view
    // modes until it is explicitly configured.
    entity_get_display($entity_type, $bundle, 'default')
      ->setComponent($field->get('name'), array(
        'label' => 'hidden',
        'type' => 'metatags_quick',
      ))
      ->save();

    if (isset($known_tags[$meta_name]['options'])) {
      $new_instance->settings['options'] = $known_tags[$meta_name]['options'];
    }
  }
}
